Beginning Research with Monica Valluri

Your first task is to do  the programing exercise on my website (https://sites.lsa.umich.edu/mvalluri/teaching/programing-exercise-for-undergraduates-working-towards-research-in-monica-valluris-group/ ) . The easiest option 
is to start with the Jupyter notebook in this folder. First make a new copy of this notebook which you will edit

What is Jupyter?:

"Jupyter notebook" is a version of Python that works through a browser
window and makes it easier to keep everything (comments, code,
figures) organized (at least for this type of an exercise).

You will need to make sure that Jupyter is installed. You can try to
do this using pip e.g. type " pip install jupyter" at the command prompt in a terminal.

Put the jupyter notebook (galaxy_exercise.ipynb) and data file
(coordAa.dat.txt) in a new folder (directory).  You will then open a
terminal window and change directory to the directory that contains
the jupyter notebook. At the prompt type "jupyter notebook" (all lower
case). This should start up a browser window. You should see the file name
displayed in the browser window.  If you click on the file it will
appear in the browser.  

To Run the Notebook:

To run the code click on the play button '|>'
(in the top menu bar) or click on the menu item "Cells" and then click
"run all" it should execute the whole note book and you should see the
first three plots that you are asked to make in the exercise.  You
should read the code boxes and the accompanying text carefully to make
sure you understand how the plots are made.  To proceed to the next
item in the exercise Click "Insert" and "Insert cell below" and add
your lines of code to the new cell. If instead you want to add some
descriptive text, add this text to the cell and click on the button
that says "Code" and click "Markdown" on the dropdown menu and then
hit the |> play button. If you enter code you can click the play
button after each line to check that you don't have any errors.  When
you are done click the play button or "Cells" "run". If your plot was
created correctly it should appear below, if not you will get error
messages which you will need to interpret and fix.

The first few steps of the exercise have been done for you. You should work through the rest of it. Ideally do each new step of the exercise in a new box.
